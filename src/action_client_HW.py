#! /usr/bin/env python3

import roslib
roslib.load_manifest('actionlib_hello_world')
import rospy
import actionlib

from actionlib_hello_world.msg import HelloWorldAction, HelloWorldGoal

def callback_active():
    rospy.loginfo("Action server is processing the goal")

def callback_feedback(msg):
    print("Processing greeting[ "+str(msg.percent_complete)+"% completed]")

def callback_done(state, result):
    if(3 == state):
        print("Hello World "+str(result.greeting)+"!")
    else:
        print("The action was not succeded")
    

if __name__ == '__main__':
    rospy.init_node('hello_world_client')
    client = actionlib.SimpleActionClient('hello_world', HelloWorldAction)
    feedback_receiver = actionlib.SimpleActionClient('hello_world', HelloWorldAction)
    client.wait_for_server()
    print("hello_world client started!")

    goal = HelloWorldGoal(greeting_id="Tienes esa risa maliciosa")
    # Fill in the goal here
    client.send_goal(goal,
                # active_cb=callback_active,
                feedback_cb=callback_feedback,
                done_cb=    callback_done)
    print("goal sent!")
    client.wait_for_result(rospy.Duration.from_sec(10.0))