#! /usr/bin/env python3

import roslib
roslib.load_manifest('actionlib_hello_world')
import rospy
import actionlib
import time

from actionlib_hello_world.msg import HelloWorldAction, HelloWorldFeedback, HelloWorldResult

class HelloWorldServer:
  def __init__(self):
    self.server = actionlib.SimpleActionServer('hello_world', HelloWorldAction, self.execute, False)
    self.server.start()
    print("hello_world server started!")

  def execute(self, goal):
    print("Server executing!")

    for i in range(1,101):
        #print("Processing greeting[ "+str(i)+"% completed]")
        # creamos el feedback para el cliente y lo mandamos
        fb = HelloWorldFeedback()
        fb.percent_complete = i
        self.server.publish_feedback(fb)
        time.sleep(1/100)
    
    rslt = HelloWorldResult(greeting = goal.greeting_id)
    self.server.set_succeeded(rslt)
    

    print("Server execution succeded!")
    #print("Hello world "+goal.greeting_id)


if __name__ == '__main__':
  rospy.init_node('hello_world_server')
  server = HelloWorldServer()
  rospy.spin()